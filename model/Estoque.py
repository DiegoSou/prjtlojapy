# FUNFANDO 1 -model/Estoque.py-
from model.Mercadoria import Mercadoria

class Estoque :
    def __init__(self) :
        self.mercadorias = {}

    def addMerc(self, merc: Mercadoria) :
        self.mercadorias.update({(len(self.mercadorias)+1) : merc})

    def calcularTotal(self) :
        total = 0.0
        for key in self.mercadorias :
            total += self.mercadorias[key].total

        return total