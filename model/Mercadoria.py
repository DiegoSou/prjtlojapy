# FUNFANDO 1 -model/Mercadoria.py-
class Mercadoria :
    def __init__(self, nome: str, quantMin: int, quantMax: int, quantAt: int, valor: float) :
        self.nome = nome
        self.quantMin = quantMin
        self.quantMax = quantMax
        self.quantAt = quantAt
        self.valor = valor
        self.total = quantAt*valor
