# FUNFANDO 1 -Main.py-

from control.AddMercadoria import addOnEstoque, store
from model.Mercadoria import Mercadoria
from prettytable import PrettyTable

addOnEstoque()

table = PrettyTable(["NOME", "QUANT MIN", "QUANT MAX", "QUANT ATUAL", "VALOR UN", "TOTAL", "REQUEST"])

for key in store.mercadorias :
    table.add_row([
        store.mercadorias[key].nome, 
        store.mercadorias[key].quantMin, 
        store.mercadorias[key].quantMax, 
        store.mercadorias[key].quantAt, 
        store.mercadorias[key].valor,
        store.mercadorias[key].total,
        "!!!" if store.mercadorias[key].quantMin >= store.mercadorias[key].quantAt else ""
        ]) 


print(table)
print("TOTAL NO ESTOQUE: ", store.calcularTotal())
