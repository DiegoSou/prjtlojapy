# mais mercadorias?

# FUNFANDO 1 -control/AddMercadoria.py-
from model.Estoque import Estoque
from model.Mercadoria import Mercadoria

store = Estoque()

def addOnEstoque():
    try:
        print("\nAdicionando no Estoque: ")

        nome = input("Nome: ")
        quantMin = int(input("Quantidade Mínima: "))
        quantMax = int(input("Quantidade Máxima: "))
        quantAt = int(input("Quantidade Atual: "))
        valor = float(input("Valor p/ Unidade: "))

        if quantAt > quantMax or valor < 0 or quantMin == 0 or quantMax == 0:
            raise Exception("Inserted values are incorrectly.")
        else:
            mec = Mercadoria(nome, quantMin, quantMax, quantAt, valor)
            store.addMerc(mec)
    except Exception as e:
        print(e)
    finally:
        if input("MAIS MERCADORIAS (S/N)? ").upper() == "S" :
            addOnEstoque()
